#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>

/**
Пример с буфером трафарета
*/
class SampleApplication : public Application
{
public:
    MeshPtr _breather;
    MeshPtr _marker;

    //Идентификатор шейдерной программы
    ShaderProgramPtr _commonShader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0f;
    float _phi = 0.0f;
    float _theta = 0.48f;
    float _spotBrightness = 1.0f;

    float _roundPhi = 0.0f;
    glm::vec3 _roundColor = glm::vec3(1.0f, 0.0f, 0.0f);

    static const int _detalizationNMin = 10;
    static const int _detalizationNMax = 1000;
    int _oldDetalizationN = 0;
    int _detalizationN = 100;
    float _breatherAlpha = 0.5;
    float _breatherOldAlpha = 0.5;

    bool _useTexture = false;
    bool _useLight = true;

    LightInfo _light;
    TexturePtr _breatherTex;

    GLuint _sampler;

    void makeBreather() {
        _breather = makeBreatherSurface(_breatherAlpha, 0.5, _detalizationN);
        _breather->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, 0.f)));
    }

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей
        _marker = makeSphere(0.1f);
        makeBreather();

        //=========================================================
        //Инициализация шейдеров
        _commonShader = std::make_shared<ShaderProgram>("494pechatnovData/common.vert", "494pechatnovData/common.frag");
        _markerShader = std::make_shared<ShaderProgram>("494pechatnovData/marker.vert", "494pechatnovData/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _breatherTex = loadTexture("494pechatnovData/brick2.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            if (ImGui::CollapsingHeader("Light")) {
                ImGui::Checkbox("enable", &_useLight);

                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

                ImGui::SliderFloat("spot brightness", &_spotBrightness, 0.0f, 1.0f);
            }
            if (ImGui::CollapsingHeader("Breather parameters")) {
                ImGui::SliderInt("polygons", &_detalizationN, _detalizationNMin, _detalizationNMax);
                ImGui::SliderFloat("alpha", &_breatherAlpha, 0.01, 0.99);
                ImGui::Checkbox("use texture", &_useTexture);
            }
        }
        ImGui::End();
    }


    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_KP_ADD || key == GLFW_KEY_EQUAL) {
                _detalizationN = std::min<int>(_detalizationNMax, _detalizationN * 1.3);
            }
            if (key == GLFW_KEY_MINUS) {
                _detalizationN = std::max<int>(_detalizationNMin, _detalizationN / 1.3);
            }
        }
    }


    void draw() override
    {
        if (_oldDetalizationN != _detalizationN || _breatherAlpha != _breatherOldAlpha) {
            _oldDetalizationN = _detalizationN;
            _breatherOldAlpha = _breatherAlpha;
            makeBreather();
        }

        _roundPhi += 0.003;
        glm::vec3 roundPositionCamSpace = glm::vec3(glm::cos(_roundPhi), glm::sin(_roundPhi), 3.0f) * 2.0f;

        //std::cout << roundPositionCamSpace.x << ", " << roundPositionCamSpace.y << ", " << roundPositionCamSpace.z << std::endl;
        //auto t = _camera.projMatrix * glm::translate(glm::mat4(1.0f), roundPositionCamSpace);
        //std::cout << t.x << ", " << t.y << ", " << t.z << ", " << t.w << std::endl;

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        //====== РИСУЕМ ОСНОВНЫЕ ОБЪЕКТЫ СЦЕНЫ ======
        _commonShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));


        _commonShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _commonShader->setVec3Uniform("light.La", _light.ambient);
        _commonShader->setVec3Uniform("light.Ld", _light.diffuse);
        _commonShader->setVec3Uniform("light.Ls", _light.specular);
        _commonShader->setVec3Uniform("light.Spot", glm::vec3(1, 1, 1) * _spotBrightness);
        _commonShader->setVec3Uniform("light.RoundPos", glm::vec3(_camera.viewMatrix * glm::vec4(glm::vec3(glm::inverse(_camera.viewMatrix) * glm::vec4(roundPositionCamSpace, 0.0f)), 1.0f)));
        _commonShader->setVec3Uniform("light.Round", _roundColor);
        _commonShader->setIntUniform("useTexture", (int)_useTexture);
        _commonShader->setIntUniform("useLight", (int)_useLight);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _breatherTex->bind();
        _commonShader->setIntUniform("diffuseTex", 0);

        {
            _commonShader->setMat4Uniform("modelMatrix", _breather->modelMatrix());
            if (_useLight) {
                _commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _breather->modelMatrix()))));
            } else {
                _commonShader->setMat3Uniform("normalToCameraMatrix", glm::mat3(1));
            }
            _breather->draw();
        }

        //Рисуем маркеры для всех источников света
        if (_useLight) {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();

            auto t = glm::vec3(glm::inverse(_camera.viewMatrix) * glm::vec4(roundPositionCamSpace, 0.0f));

            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), t));
            _markerShader->setVec4Uniform("color", glm::vec4(_roundColor, 1.0f));
            _marker->draw();

            //~ _markerShader->use();
            //~ _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * glm::translate(glm::mat4(1.0f), roundPositionCamSpace));
            //~ _markerShader->setVec4Uniform("color", glm::vec4(_roundColor, 1.0f));
            //~ _marker->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
