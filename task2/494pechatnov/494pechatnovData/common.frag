#version 330

uniform sampler2D diffuseTex;
uniform int useTexture;

struct LightInfo
{
    vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света от 1 подвижного источника
    vec3 Ls; //цвет и интенсивность бликового света от 1 подвижного источника
    vec3 Spot; // цвет и интенсивность 2 источника на камере
    vec3 RoundPos; // положение 3 источника
    vec3 Round; // цвет и интенсивность 3 источника, который вращается в плоскости камеры вокруг объекта
};
uniform LightInfo light;
uniform int useLight;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord;

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;



void main()
{
    vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
    vec3 diffuseColor = normal * 0.5 + 0.5;

    if (useTexture != 0) {
        diffuseColor = texture(diffuseTex, texCoord).rgb;
    }

    if (useLight != 0) {
        vec3 resultColor = diffuseColor * light.La;
        // 1 источник
        {
            vec3 lightDirCamSpace = light.pos - posCamSpace.xyz; //направление на источник света
            float distance = length(lightDirCamSpace);
            lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

            if (dot(normal, lightDirCamSpace.xyz) < 0) {
                normal = -normal;
            }

            float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

            resultColor += diffuseColor * light.Ld * NdotL / (distance * distance) * 10;

            if (NdotL > 0.0)
            {
                vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
                vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

                float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
                blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

                resultColor += light.Ls * Ks * blinnTerm;
            }
        }
        // 2 источник
        {
            vec3 lightDirCamSpace = -posCamSpace.xyz; //направление на источник света
            float distance = length(lightDirCamSpace);
            lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

            if (dot(normal, lightDirCamSpace.xyz) < 0) {
                normal = -normal;
            }

            float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

            //~ if (NdotL > 0) {
                //~ resultColor += vec3(1, 0, 0);
            //~ }

            //~ if (lightDirCamSpace.z > 0.99) {
                //~ resultColor += vec3(1, 0, 0);
            //~ }

            float radialCoef = max(lightDirCamSpace.z - cos(0.1), 0) / (1.0 - cos(0.1));

            resultColor += diffuseColor * light.Spot * radialCoef * NdotL / (distance * distance) * 10;

            if (NdotL > 0.0)
            {
                vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
                vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

                float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
                blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

                resultColor += light.Spot * Ks * blinnTerm;
            }
        }

        // 3 источник
        {
            vec3 lightDirCamSpace = light.RoundPos - posCamSpace.xyz; //направление на источник света
            float distance = length(lightDirCamSpace);
            lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

            if (dot(normal, lightDirCamSpace.xyz) < 0) {
                normal = -normal;
            }

            float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

            resultColor += light.Round * NdotL / (distance * distance) * 10;

            if (NdotL > 0.0)
            {
                vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
                vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

                float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
                blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика

                resultColor += light.Round * Ks * blinnTerm;
            }
        }

        //resultColor += vec3(-normalize(posCamSpace.xyz).z, 0, 0);


        fragColor = vec4(resultColor, 0.5);
    } else {
        fragColor = vec4(diffuseColor, 0.5);
    }
}
