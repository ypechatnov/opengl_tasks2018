#include <Application.hpp>
#include <LightInfo.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include <iostream>
#include <sstream>
#include <vector>

/**
Пример с тенями
*/
class SampleApplication : public Application
{
public:

    MeshPtr _breather;
    MeshPtr _ground;

    MeshPtr _quad;

    MeshPtr _marker; //Меш - маркер для источника света

    //Идентификатор шейдерной программы
    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _quadShader;
    ShaderProgramPtr _renderToShadowMapShader;
    ShaderProgramPtr _commonShader;
    ShaderProgramPtr _commonShaderVar2;

    //Переменные для управления положением одного источника света
    float _lr = 7.0f;
    float _phi = 0.0f;
    float _theta = 1.48f;

    float _spotBrightness = 1.0f;

    float _roundPhi = 0.0f;
    glm::vec3 _roundColor = glm::vec3(1.0f, 0.0f, 0.0f);

    // breather params
    static const int _detalizationNMin = 10;
    static const int _detalizationNMax = 1000;
    int _oldDetalizationN = 0;
    int _detalizationN = 100;
    float _breatherAlpha = 0.5;
    float _breatherOldAlpha = 0.5;

    LightInfo _light;
    CameraInfo _lightCamera;

    TexturePtr _brickTex;

    GLuint _sampler;
    GLuint _depthSampler;
    GLuint _depthSamplerLinear;

    GLuint _framebufferId;
    GLuint _depthTexId;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;

    bool _showDepthQuad = false;
    bool _isLinearSampler = false;
    bool _cullFrontFaces = false;
    bool _useTexture = false;
    bool _useLight = true;

    void initFramebuffer()
    {
        //Создаем фреймбуфер
        glGenFramebuffers(1, &_framebufferId);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        //----------------------------

        //Создаем текстуру, куда будем впоследствии копировать буфер глубины
        glGenTextures(1, &_depthTexId);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, _fbWidth, _fbHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexId, 0);

        //----------------------------

        //Указываем куда именно мы будем рендерить
        GLenum buffers[] = { GL_NONE };
        glDrawBuffers(1, buffers);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void makeBreather() {
        _breather = makeBreatherSurface(_breatherAlpha, 0.5, _detalizationN);
        _breather->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, 0.f)));
    }


    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

        makeBreather();

        _ground = makeGroundPlane(5.0f, 2.0f);
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.f, 0.f, -2.1f)));

        _marker = makeSphere(0.1f);

        _quad = makeScreenAlignedQuad();

        //=========================================================
        //Инициализация шейдеров

        _markerShader = std::make_shared<ShaderProgram>("494pechatnovData/marker.vert", "494pechatnovData/marker.frag");
        _quadShader = std::make_shared<ShaderProgram>("494pechatnovData/quadDepth.vert", "494pechatnovData/quadDepth.frag");
        _renderToShadowMapShader = std::make_shared<ShaderProgram>("494pechatnovData/toshadow.vert", "494pechatnovData/toshadow.frag");
        _commonShader = std::make_shared<ShaderProgram>("494pechatnovData/common.vert", "494pechatnovData/common.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _brickTex = loadTexture("494pechatnovData/brick2.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

        glGenSamplers(1, &_depthSampler);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glSamplerParameterfv(_depthSampler, GL_TEXTURE_BORDER_COLOR, border);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        glGenSamplers(1, &_depthSamplerLinear);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glSamplerParameterfv(_depthSamplerLinear, GL_TEXTURE_BORDER_COLOR, border);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        //=========================================================
        //Инициализация фреймбуфера для рендера теневой карты

        initFramebuffer();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

                ImGui::SliderFloat("spot brightness", &_spotBrightness, 0.0f, 1.0f);
            }

            if (ImGui::CollapsingHeader("Breather parameters")) {
                ImGui::SliderInt("polygons", &_detalizationN, _detalizationNMin, _detalizationNMax);
                ImGui::SliderFloat("alpha", &_breatherAlpha, 0.01, 0.99);
            }

            ImGui::Checkbox("Show depth quad", &_showDepthQuad);
            ImGui::Checkbox("Linear sampler", &_isLinearSampler);
            ImGui::Checkbox("Cull front faces", &_cullFrontFaces);

            ImGui::Checkbox("Use light", &_useLight);
            ImGui::Checkbox("Use texture", &_useTexture);
        }
        ImGui::End();
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_Z)
            {
                _showDepthQuad = !_showDepthQuad;
            }
            else if (key == GLFW_KEY_L)
            {
                _isLinearSampler = !_isLinearSampler;
            }
            else if (key == GLFW_KEY_C)
            {
                _cullFrontFaces = !_cullFrontFaces;
            }
            else if (key == GLFW_KEY_KP_ADD || key == GLFW_KEY_EQUAL) {
                _detalizationN = std::min<int>(_detalizationNMax, _detalizationN * 1.3);
            }
            else if (key == GLFW_KEY_MINUS) {
                _detalizationN = std::max<int>(_detalizationNMin, _detalizationN / 1.3);
            }
        }
    }

    void update()
    {
        Application::update();

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _lightCamera.viewMatrix = glm::lookAt(_light.position, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        _lightCamera.projMatrix = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 30.f);
    }

    void draw() override
    {
        drawToShadowMap(_lightCamera);
        drawToScreen(_commonShader, _camera, _lightCamera);

        if (_showDepthQuad)
        {
            drawDebug();
        }
    }

    void drawToShadowMap(const CameraInfo& lightCamera)
    {
        //=========== Сначала подключаем фреймбуфер и рендерим в текстуру ==========
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        glViewport(0, 0, _fbWidth, _fbHeight);
        glClear(GL_DEPTH_BUFFER_BIT);

        _renderToShadowMapShader->use();
        _renderToShadowMapShader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
        _renderToShadowMapShader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

        if (_cullFrontFaces)
        {
            glEnable(GL_CULL_FACE);
            glFrontFace(GL_CW);
            glCullFace(GL_FRONT);
        }

        drawScene(_renderToShadowMapShader, lightCamera);

        if (_cullFrontFaces)
        {
            glDisable(GL_CULL_FACE);
        }

        glUseProgram(0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0); //Отключаем фреймбуфер
    }

    void drawToScreen(const ShaderProgramPtr& shader, const CameraInfo& camera, const CameraInfo& lightCamera)
    {
        _roundPhi += 0.003;
        glm::vec3 roundPositionCamSpace = glm::vec3(glm::cos(_roundPhi), glm::sin(_roundPhi), 3.0f) * 2.0f;

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

        glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_light.position, 1.0));

        shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform("light.La", _light.ambient);
        shader->setVec3Uniform("light.Ld", _light.diffuse);
        shader->setVec3Uniform("light.Ls", _light.specular);
        shader->setVec3Uniform("light.Spot", glm::vec3(1, 1, 1) * _spotBrightness);
        shader->setVec3Uniform("light.RoundPos", glm::vec3(_camera.viewMatrix * glm::vec4(glm::vec3(glm::inverse(_camera.viewMatrix) * glm::vec4(roundPositionCamSpace, 0.0f)), 1.0f)));
        shader->setVec3Uniform("light.Round", _roundColor);

        {
            shader->setMat4Uniform("lightViewMatrix", lightCamera.viewMatrix);
            shader->setMat4Uniform("lightProjectionMatrix", lightCamera.projMatrix);

            glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
            shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _brickTex->bind();
        shader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glBindSampler(1, _isLinearSampler ? _depthSamplerLinear : _depthSampler);
        shader->setIntUniform("shadowTex", 1);

        shader->setIntUniform("useTexture", (int)_useTexture);
        shader->setIntUniform("useLight", (int)_useLight);

        drawScene(shader, camera);

        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", camera.projMatrix * camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();

            auto t = glm::vec3(glm::inverse(_camera.viewMatrix) * glm::vec4(roundPositionCamSpace, 0.0f));

            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), t));
            _markerShader->setVec4Uniform("color", glm::vec4(_roundColor, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void drawScene(const ShaderProgramPtr& shader, const CameraInfo& camera)
    {
        shader->setMat4Uniform("modelMatrix", _breather->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _breather->modelMatrix()))));

        _breather->draw();

        shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _ground->modelMatrix()))));

        _ground->draw();
    }

    void drawDebug()
    {
        glViewport(0, 0, 500, 500);

        _quadShader->use();

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glBindSampler(0, _sampler);
        _quadShader->setIntUniform("tex", 0);

        _quad->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
